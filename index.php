<?php
    $side = "";
    if(isset($_GET['side'])){
		$side = $_GET['side'].".php";
    }
?>
<html lang="nn">
	<head>
        <title>Hjørundfjord</title>
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="stylesheet" type="text/css" href="stylesheets/main.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        
    </head>
    
	<body id="top">
        <header class="l-site-header headroom" id="header">
            <h1>hjorundfjord.no</h1>
            <i class="fas fa-search"></i>
            <i class="fas fa-user"></i>
        </header>
        <header class="nav-down">
            This is your menu.
        </header>
        <nav class="l-main-nav">
            <ul>
                <li><a href="?side=main" class="active"><i class="fas fa-home"></i> Hovudside</a></li>
                <li><a href="/"><i class="fas fa-newspaper"></i> Nyhende</a></li>
                <li><a href="/"><i class="fas fa-calendar-alt"></i> Aktivitetar</a></li>
                <li><a href="/"><i class="fas fa-info-circle"></i> Info</a></li>
            </ul>
        </nav>
        <main class="l-site-content">
            <?php
            if(isset($_GET['side'])){
                include('content/'.$side);
            }
            ?>
        </main>
        
        <footer class="l-site-footer">
            <ul>
                <li><a href="/" title="Kontakt oss"><i class="far fa-envelope"></i> <span>Kontakt oss</span></a></li>
                <li><a href="/" title="Nettstadskart"><i class="fas fa-sitemap"></i> <span>Nettstadskart</span></a></li>
                <li><a href="/" title="Rapportere feil"><i class="fas fa-bug"></i> <span>Rapportere feil</span></a></li>
                <li><a href="/" title="RSS-feed"><i class="fas fa-rss-square"></i> <span>RSS</span></a></li>
                <li><a href="#top"><i class="fas fa-arrow-up"></i><span>Til toppen</span></a></li>
            </ul>
        </footer>

        <!-- Headroom -->
        <script src="js/headroom.js"></script>
        <script>
            (function() {
                var header = document.querySelector("#header");

                if(window.location.hash) {
                    header.classList.add("headroom--unpinned");
                }

                var headroom = new Headroom(header, {
                    tolerance: {
                        down : 10,
                        up : 20
                    },
                    offset : 205
                });
                headroom.init();

            }());
        </script>
        <!-- Headroom end -->
	</body>
</html>